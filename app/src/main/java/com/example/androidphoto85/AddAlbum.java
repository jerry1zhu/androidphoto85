package com.example.androidphoto85;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class AddAlbum extends AppCompatActivity {

    private EditText albumName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_album);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        albumName =findViewById(R.id.album_name);
    }

    public void add(View view){
        String name = albumName.getText().toString();
        if (name == null || name.length() == 0) {
            return; // does not quit activity, just returns from method
        }
        Log.d("test",name);
        Bundle bundle = new Bundle();
        bundle.putString("albumName",name);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_OK,intent);
        finish();
    }

}
