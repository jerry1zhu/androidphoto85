package com.example.androidphoto85;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class OpenEditDeleteAlbum extends AppCompatActivity {

    public Session session;
    public Album album;
    public static final String album_name = "albumName";
    public static final String album_index = "albumIndex";
    private EditText albumName;
    private int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.open_edit_delete_album);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        albumName =findViewById(R.id.album_name);
        Bundle bundle = getIntent().getExtras();

        session = (Session) bundle.getSerializable("session");
        index = session.album_index;
        album = session.albums.get(index);
        albumName.setText(album.name);


    }

    public void open(View view){
        Bundle bundle = new Bundle();
        bundle.putSerializable("session", session);
        Intent i = new Intent(OpenEditDeleteAlbum.this,AlbumHomePage.class);
        i.putExtras(bundle);
        startActivityForResult(i,123);

        bundle = new Bundle();
        bundle.putString(album_name,albumName.getText().toString());
        bundle.putInt(album_index,index);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(4,intent);
        finish();
    }

    public void save(View view){
        String name =albumName.getText().toString();
        if (name == null || name.length() == 0) {
            return; // does not quit activity, just returns from method
        }
        Bundle bundle = new Bundle();
        bundle.putString(album_name,name);
        bundle.putInt(album_index,index);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_OK,intent);
        finish();
    }

    public void delete(View view){
        Bundle bundle = new Bundle();
        bundle.putInt(album_index,index);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(2,intent);
        finish();
    }
}
