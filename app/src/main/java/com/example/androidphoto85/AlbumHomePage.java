package com.example.androidphoto85;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;

import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

class ImageAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Bitmap> bitmapList;

    public ImageAdapter(Context context, ArrayList<Bitmap> bitmapList) {
        this.context = context;
        this.bitmapList = bitmapList;
    }

    public int getCount() {
        return this.bitmapList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(this.context);
            imageView.setLayoutParams(new GridView.LayoutParams(240, 240));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setImageBitmap(this.bitmapList.get(position));

        return imageView;
    }

}

public class AlbumHomePage extends AppCompatActivity {

    private GridView imageGrid;
    private ArrayList<Bitmap> bitmapList;
    private ArrayList<Album> albums;
    private Album album;
    public Session session;

    Uri test;
    int album_index;
    Album currAlbum;
    private String selectedImagePath;
    private String filemanagerstring;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getIntent().getExtras() != null){
            Bundle extras = getIntent().getExtras();
        }

        setContentView(R.layout.activity_album_home_page);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.imageGrid = (GridView) findViewById(R.id.gridview);
        this.bitmapList = new ArrayList<Bitmap>();

        Bundle bundle = getIntent().getExtras();

        session = (Session) bundle.getSerializable("session");
        albums = session.albums;
        album_index = session.album_index;
        album = albums.get(album_index);
        if(session.photo_index != -1) open(session.photo_index);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    124);
            return;
        }

        setTitle(album.name);

        setBitmap(album);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, 124);
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setBitmap(Album currAlbum){
        this.currAlbum = currAlbum;
        this.bitmapList = new ArrayList<>();

        try {
            for(int i = 0; i < currAlbum.photos.size(); i++) {
                this.bitmapList.add(urlImageToBitmap(currAlbum.photos.get(i).path));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.imageGrid.setAdapter(new ImageAdapter(this,this.bitmapList));

        imageGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                open(position);
            }
        });
    }
    private Bitmap urlImageToBitmap(String path) throws Exception {
        Log.d("ImageTest",path);
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        return bitmap;
    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        //Log.d("Something","working");
        if (resultCode == RESULT_OK) {
            Uri imageUri = data.getData();
            filemanagerstring = imageUri.getPath();
            selectedImagePath = getPath(imageUri);
            String path;
            if(selectedImagePath!=null){
                path = selectedImagePath;
            }
            else {
                path = filemanagerstring;
            }

            Photo newPhoto = new Photo("name",path);
            session.albums.get(album_index).addPhoto(newPhoto);
            setBitmap(currAlbum);
        }


        if (resultCode == 1){
            Bundle bundle = getIntent().getExtras();
            session = (Session)  bundle.getSerializable("session");
            Log.d("Return", "onActivityResult: Return from photo");
            Log.d("Return","1.5"+session.albums.get(0).photos.get(0).tags.toString());
            //session.albums = (ArrayList<Album>) data.getExtras().get("albums");
            loadSession();
            setBitmap((Album) data.getExtras().get("album"));
            session.photo_index = -1;
        }
        Log.d("test", "saving");
        saveSession();
        Log.d("Return","1.75"+session.albums.get(0).photos.get(0).tags.toString());
    }
    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if(cursor!=null)
        {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        else return null;
    }

    public void open(int position){
        session.photo_index = position;
        Bundle bundle = new Bundle();
        bundle.putSerializable("session", session);
        Intent i = new Intent(AlbumHomePage.this,PhotoHome.class);
        i.putExtras(bundle);
        //edit album information is for 1
        startActivityForResult(i, 1);



    }

    public void saveSession(){
        try {
            FileOutputStream fileOutputStream = openFileOutput("session.dat", MODE_PRIVATE);
            ObjectOutputStream out = new ObjectOutputStream(fileOutputStream);
            out.writeObject(session);
            out.close();
            fileOutputStream.close();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public Session loadSession(){
        session = new Session();

        try {
            FileInputStream inputStream = openFileInput("session.dat");
            ObjectInputStream in = new ObjectInputStream(inputStream);
            session = (Session) in.readObject();
            in.close();
            inputStream.close();
        } catch (IOException e) {

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        saveSession();

        return session;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 124: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.d("Perm","Permission Granted");
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("session",session);
                    Intent intent = new Intent(AlbumHomePage.this,AlbumHomePage.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("session",session);
                    Intent intent = new Intent(AlbumHomePage.this,MainActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(AlbumHomePage.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

}
