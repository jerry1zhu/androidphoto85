package com.example.androidphoto85;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

class Tag implements Serializable{
    public String name;
    public String val;

    Tag(String name, String val) {
        this.name = name;
        this.val = val;
    }

    public String toString() {
        return name + " = " + val;
    }
}

class Photo implements Serializable {

    String name;
    String path;
    ArrayList<Tag> tags;

    Photo(String name,String path){
        this.name = name;
        this.path = path;
        this.tags = new ArrayList<>();
    }

    public String toString(){
        return name + "|" + path;
    }
}

class Album implements Serializable {
    String name;
    ArrayList<Photo> photos;
    Album(String name){
        this.name = name;
        photos = new ArrayList<Photo>();
    }
    public void addPhoto(Photo newPhoto){
        photos.add(newPhoto);
    }

    public void removePhoto(int i){
        photos.remove(i);
    }

    public String toString(){
        return name;
    }
}

class Session implements Serializable{
    ArrayList<Album> albums;
    int album_index;
    int photo_index;

    public Session(){
        this.albums = new ArrayList<>();
        album_index = -1;
        photo_index = -1;

        String[] albumList = {"Stock Pictures", "My Album"};
        for(int i=0;i<albumList.length;i++){
            this.addAlbum(new Album(albumList[i]));
        }

    }
    public void addAlbum(Album a){
        albums.add(a);
    }

    public void save(){
        try {
            File file = new  File("session.data");
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            ObjectOutputStream out = new ObjectOutputStream(fileOutputStream);
            out.writeObject(this);
            out.close();
            fileOutputStream.close();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private ArrayList<Album> albums;
    public Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        session = loadSession();
        albums = session.albums;
        listView = findViewById(R.id.list_of_albums);
        listView.setAdapter(new ArrayAdapter<Album>(this,R.layout.album,albums));

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("session", session);
                Intent intent = new Intent(MainActivity.this, SearchController.class);
                intent.putExtras(bundle);
                //search return is 5
                startActivityForResult(intent,5);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Album selectedItem = (Album) parent.getItemAtPosition(position);
                Bundle bundle = new Bundle();
                session.album_index = position;

                bundle.putSerializable("session", session);
                Intent i = new Intent(MainActivity.this,OpenEditDeleteAlbum.class);
                i.putExtras(bundle);
                //edit album information is for 1
                startActivityForResult(i,1);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.add_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode, Intent intent){

        Bundle bundle = intent.getExtras();
        if(bundle == null){
            Log.d("Return","Nothing Happened!");
            return;
        }
        Log.d("Return","WE RETURNED TO THE MAIN PAGE");
        String name =bundle.getString(OpenEditDeleteAlbum.album_name);
        int index = bundle.getInt(OpenEditDeleteAlbum.album_index);

        if(resultCode == 4){
            Log.d("Return","open photo");
            session.album_index = -1;
        }

        if(resultCode == 2){
            Album album = session.albums.get(index);
            session.albums.remove(album);
        }
        else {
            if (requestCode == 1) {
                Album album = session.albums.get(index);
                album.name = name;
            }
            if(requestCode == 2){
                session.albums.add(new Album(name));
            }
        }

        saveSession();


        listView.setAdapter(new ArrayAdapter<Album>(this,R.layout.album,albums));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:

                addAlbum();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void addAlbum(){
        Intent intent = new Intent(this, AddAlbum.class);
        startActivityForResult(intent,2);
    }

    public void saveSession(){

        try {
            System.out.println(session.albums.get(1).photos);
            FileOutputStream fileOutputStream = openFileOutput("session.dat", MODE_PRIVATE);
            ObjectOutputStream out = new ObjectOutputStream(fileOutputStream);
            out.writeObject(session);
            out.close();
            fileOutputStream.close();
            Log.d("saved","true");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public Session loadSession(){
        session = new Session();

        try {
            FileInputStream inputStream = openFileInput("session.dat");
            ObjectInputStream in = new ObjectInputStream(inputStream);
            session = (Session) in.readObject();
            in.close();
            inputStream.close();
        } catch (IOException e) {

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        saveSession();

        return session;
    }
}

