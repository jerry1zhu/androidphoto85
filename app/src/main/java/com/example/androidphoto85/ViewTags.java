package com.example.androidphoto85;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class ViewTags extends AppCompatActivity{

    public Session session;
    public ArrayList<Album> albums;
    public ArrayList<Photo> photos;
    public ArrayList<Tag> tags;
    public Album album;
    public Photo photo;
    public int albumIndex;
    public int photoIndex;

    private ListView listView;
    private Spinner spinner;
    private EditText editText;
    private int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_tags);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        session = (Session) bundle.getSerializable("session");
        albums = session.albums;
        albumIndex = session.album_index;
        photoIndex = session.photo_index;
        album = albums.get(albumIndex);
        photos = album.photos;
        photo = photos.get(photoIndex);
        tags = photo.tags;

        listView = findViewById(R.id.list_of_tags);
        listView.setAdapter(new ArrayAdapter<Tag>(this,R.layout.tags,tags));

        spinner = findViewById(R.id.spinner1);
        List<String> choices = new ArrayList<String>();
        choices.add("Person");
        choices.add("Location");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,choices);
        spinner.setAdapter(dataAdapter);

        editText = findViewById(R.id.tag_value);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("Tag",""+position);
                pos = position;
                //using the position get the tags from the photo
                Tag tag = photo.tags.get(position);
                //update the Spinner choice name to match the select item
                if(tag.name.equals("Person")){
                    spinner.setSelection(0);
                }
                else{
                    spinner.setSelection(1);
                }
                //set the text to the field for tag
                editText.setText(tag.val);
            }
        });

    }


    public void create(View view){
        String type;
        String value;

        if(spinner.getSelectedItemPosition() == 0){
            type = "Person";
        }else{
            type = "Location";
        }
        value = editText.getText().toString();

        //photo.tags.add(new Tag(type,value));
        session.albums.get(albumIndex).photos.get(photoIndex).tags.add(new Tag(type,value));
        saveSession();

        listView.setAdapter(new ArrayAdapter<Tag>(this,R.layout.tags,tags));
    }

    public void save(View view){
        //update the tag
        String type,value;
        if(spinner.getSelectedItemPosition() == 0){
            type = "Person";
        }else{
            type = "Location";
        }
        value = editText.getText().toString();

        session.albums.get(albumIndex).photos.get(photoIndex).tags.get(pos).name = type;
        session.albums.get(albumIndex).photos.get(photoIndex).tags.get(pos).val = value;

        saveSession();

        listView.setAdapter(new ArrayAdapter<Tag>(this,R.layout.tags,tags));
    }

    public void delete(View view){
        //if there is something to delete
        if(photo.tags.size() != 0){
            session.albums.get(albumIndex).photos.get(photoIndex).tags.remove(pos);
            saveSession();
        }
        listView.setAdapter(new ArrayAdapter<Tag>(this,R.layout.tags,tags));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;

    }

    @Override
    public void finish() {
        Bundle bundle = new Bundle();
        bundle.putSerializable("session", session);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        Log.d("Return","2"+session.albums.get(0).photos.get(0).tags.toString());
        saveSession();
        setResult(5,intent);
        super.finish();
    }

    public void saveSession(){

        try {
            FileOutputStream fileOutputStream = openFileOutput("session.dat", MODE_PRIVATE);
            ObjectOutputStream out = new ObjectOutputStream(fileOutputStream);
            out.writeObject(session);
            out.close();
            fileOutputStream.close();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
