package com.example.androidphoto85;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class PhotoHome extends AppCompatActivity {

    public Session session;
    public  ArrayList<Album> albums;
    public ArrayList<Photo> photos;
    public Album album;
    public int albumIndex;
    public int photoIndex;


    private ImageView imageView;
    private Photo displayed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.imageView = (ImageView) findViewById(R.id.imageView1);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        Bundle bundle = getIntent().getExtras();
        session = (Session) bundle.getSerializable("session");
        albums = session.albums;
        albumIndex = session.album_index;
        photoIndex = session.photo_index;
        album = albums.get(albumIndex);
        photos = album.photos;
        openPhoto(photoIndex);

    }

    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        //Log.d("Something","working");

        if (resultCode == 5) {
            session =(Session) data.getExtras().getSerializable("session");
            loadSession();
            saveSession();
        }

    }
    public void openPhoto(int i){
        displayed = photos.get(photoIndex);
        String path = displayed.path;
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        imageView.setImageBitmap(bitmap);
    }

    public void nextPhoto(View view){
        if (photoIndex >= photos.size() -1) photoIndex = 0;
        else photoIndex += 1;
        openPhoto(photoIndex);
    }

    public void prevPhoto(View view){
        if (photoIndex == 0) photoIndex = photos.size() -1;
        else photoIndex -= 1;
        openPhoto(photoIndex);
    }

    public void move(View view){
        final Spinner spinner = new Spinner(this);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<Album> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, albums);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        spinner.setLayoutParams(new ViewGroup.LayoutParams(100, ViewGroup.LayoutParams.WRAP_CONTENT));

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Destination Album")

                .setPositiveButton(android.R.string.yes, new DialogInterface .OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        commitMove((Album) spinner.getSelectedItem());
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert);

        builder.setView(spinner);
        builder.create().show();
    }

    public void commitMove(Album a){
        if (a == album){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Same Album Move")
                    .setMessage("You must move the photo to a different album than where it started")
                    .setPositiveButton(android.R.string.yes, null)

                    // A null listener allows the button to dismiss the dialog and take no further action.
                    .setCancelable(false)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }

        else{
            a.addPhoto(photos.get(photoIndex));
            commitDelete();
        }
    }

    public void delete(View view){
        new AlertDialog.Builder(this)
                .setTitle("Delete photo")
                .setMessage("Are you sure you want to delete this photo?")

                .setPositiveButton(android.R.string.yes, new DialogInterface .OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        commitDelete();
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void commitDelete(){
        album.removePhoto(photoIndex);
        try {
            FileOutputStream fileOutputStream = openFileOutput("albums.dat", Context.MODE_PRIVATE);
            ObjectOutputStream out = new ObjectOutputStream(fileOutputStream);
            out.writeObject(albums);
            out.close();
            fileOutputStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (photos.size() == 0) finish();
        else nextPhoto(findViewById(R.id.next));
    }

    public void tags(View view){
        Log.d("Test","Tag button");
        Bundle bundle = new Bundle();
        bundle.putSerializable("session", session);
        Intent intent = new Intent(PhotoHome.this,ViewTags.class);
        intent.putExtras(bundle);
        startActivityForResult(intent,5);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;

    }

    @Override
    public void finish() {
        Intent result = new Intent();
        result.putExtra("album", album);
        result.putExtra("albums", albums);
        Log.d("Return","2"+session.albums.get(0).photos.get(0).tags.toString());
        saveSession();
        setResult(1, result);
        super.finish();
    }

    public void saveSession(){

        try {
            FileOutputStream fileOutputStream = openFileOutput("session.dat", MODE_PRIVATE);
            ObjectOutputStream out = new ObjectOutputStream(fileOutputStream);
            out.writeObject(session);
            out.close();
            fileOutputStream.close();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public Session loadSession(){
        session = new Session();

        try {
            FileInputStream inputStream = openFileInput("session.dat");
            ObjectInputStream in = new ObjectInputStream(inputStream);
            session = (Session) in.readObject();
            in.close();
            inputStream.close();
        } catch (IOException e) {

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        saveSession();

        return session;
    }
}
