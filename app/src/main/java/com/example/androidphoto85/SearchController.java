package com.example.androidphoto85;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import java.util.ArrayList;

public class SearchController extends AppCompatActivity {

    public Session session;
    public int tags = 2;

    private LinearLayout container1;
    private LinearLayout container2;
    private Spinner conjunction;
    private Spinner tag1;
    private Spinner tag2;
    private EditText val1;
    private EditText val2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle bundle = getIntent().getExtras();

        container1 = findViewById(R.id.container1);
        container2 = findViewById(R.id.container2);
        conjunction = findViewById(R.id.conjuction);
        tag1 = findViewById(R.id.tag1);
        tag2 = findViewById(R.id.tag2);
        val1 = findViewById(R.id.val1);
        val2 = findViewById(R.id.val2);


        String[] tags = {"Person", "Location"};
        String[] cons = {"and", "or"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, tags);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, cons);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        tag1.setAdapter(adapter);
        tag2.setAdapter(adapter);
        conjunction.setAdapter(adapter2);

        session = (Session) bundle.getSerializable("session");
    }

    public void oneTag(View view){
        container2.setVisibility(View.GONE);
        conjunction.setVisibility(View.GONE);
        tags = 1;
    }

    public void twoTags(View view){
        container2.setVisibility(View.VISIBLE);
        conjunction.setVisibility(View.VISIBLE);
        tags = 2;
    }

    public void search(View view){
        ArrayList<Photo> results = new ArrayList<>();
        for(Album a : session.albums){
            for(Photo p : a.photos){
                if(mathchesTags(p)) results.add(p);
                System.out.println(p.tags);
            }
        }

        openResults(results);
    }

    public void openResults(ArrayList<Photo> photos){
        Bundle bundle = new Bundle();
        bundle.putSerializable("results", photos);
        Intent i = new Intent(SearchController.this, ResultsPage.class);
        i.putExtras(bundle);
        //edit album information is for 1
        startActivity(i);
    }

    public boolean mathchesTags(Photo p){

        if(tags == 1) return singleTagSearch(p);
        else return doubleTagSearch(p);
    }

    public boolean singleTagSearch(Photo p){
        for(Tag t : p.tags){
            String pname = t.name.toLowerCase().trim();
            String sname = tag1.getSelectedItem().toString().toLowerCase().trim();
            String pval = t.val.toLowerCase().trim();
            String sval = val1.getText().toString().toLowerCase().trim();

            if(pname.equals(sname) && pval.contains(sval) ) return true;
        }
        return false;
    }

    public boolean doubleTagSearch(Photo p){
        Boolean first = false;
        Boolean second = false;

        for(Tag t: p.tags) {
            String pname = t.name.toLowerCase();
            String sname1 = tag1.getSelectedItem().toString().toLowerCase().trim();
            String sname2 = tag2.getSelectedItem().toString().toLowerCase().trim();
            String pval = t.val.toLowerCase().trim();
            String sval1 = val1.getText().toString().toLowerCase().trim();
            String sval2 = val2.getText().toString().toLowerCase().trim();

            //If the key is the same and the value is the same
            if(pname.equals(sname1) && pval.contains(sval1) ){
                first = true;
            }

            if(pname.equals(sname2) && pval.contains(sval2)) {
                second = true;
            }
        }

        if(conjunction.getSelectedItem().toString().equals("or")) {
            return first || second;
        }
        else return first && second;
    }




}
